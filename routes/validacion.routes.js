const { Router } = require("express");

let validacion = Router();

var bd = require('../controllers/validacion.controllers');


validacion.put("/:id", bd.actualizar);
validacion.get("/", bd.getProspectos);

module.exports = validacion;