const { Router } = require("express");

let prospectos = Router();

var bd = require('../controllers/prospectos.controllers');

prospectos.get("/", bd.getProspectos);
prospectos.post("/", bd.create);

module.exports = prospectos;