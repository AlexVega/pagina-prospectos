const { Router } = require("express");

let listado = Router();

var bd = require('../controllers/listado.controllers');

listado.get("/", bd.getListado);
listado.get("/busqueda", bd.search);
listado.get("/:id", bd.findyById);

module.exports = listado;