const client = require('./conection')
conn = client.createConnection();

//const shared = require('./shared');
let jResponse = {};


//GET Obtener postulante
exports.getListado = function(req, res) {

    const query = `SELECT id,nombre, apellidopaterno,apellidomaterno,calle, numero, colonia, codigo_postal, telefono, rfc, estatus, observaciones,documentos FROM prospectos`;

    conn.query(query, (err, resultado) => {

        if (err) {

            console.log("****************ERROR EN LA CONSULTA****************");
            console.log(err);
            console.log(query);
            console.log("****************ERROR EN LA CONSULTA****************");
            jResponse = { status: 400, message: "Ocurrió un error" }
        } else {
            jResponse = { status: 200, message: "Exitoso", data: resultado }
        }
        res.status(200).json(jResponse);

    });

}

//GET => Find  by id
exports.findyById = function(req, res) {

    let query = "";

    query = `SELECT id, nombre, apellidopaterno, apellidomaterno, calle, numero, colonia, codigo_postal, telefono, rfc, estatus, documentos,observaciones FROM prospectos where id = ${req.params.id}`;

    conn.query(query, (err, resultado) => {

        if (err) {

            console.log("****************ERROR EN LA CONSULTA****************");
            console.log(err);
            console.log(query);
            console.log("****************ERROR EN LA CONSULTA****************");
            jResponse = { status: 400, message: "Ocurrió un error" }
        } else {
            jResponse = { status: 200, message: "Exitoso", data: resultado }
        }
        res.status(200).json(jResponse);

    });

}


exports.search = function(req, res) {
    let query = "";

    query = `SELECT nombre, apellidopaterno, apellidomaterno, estatus FROM prospectos`;

    conn.query(query, (err, resultado) => {

        if (err) {

            console.log("****************ERROR EN LA CONSULTA****************");
            console.log(err);
            console.log(query);
            console.log("****************ERROR EN LA CONSULTA****************");
            jResponse = { status: 400, message: "Ocurrió un error" }
        } else {
            jResponse = { status: 200, message: "Exitoso", data: resultado }
        }
        res.status(200).json(jResponse);

    });
}