const client = require('./conection')
conn = client.createConnection();

//const shared = require('./shared');
let jResponse = {};


//GET Obtener postulante
exports.getProspectos = function(req, res) {

    const query = `SELECT id,nombre, apellidopaterno,apellidomaterno,calle, numero, colonia, codigo_postal, telefono, rfc, estatus, observaciones,documentos FROM prospectos`
    conn.query(query, (err, resultado) => {

        if (err) {

            console.log("****************ERROR EN LA CONSULTA****************");
            console.log(err);
            console.log(query);
            console.log("****************ERROR EN LA CONSULTA****************");
            jResponse = { status: 400, message: "Ocurrió un error" }
        } else {
            jResponse = { status: 200, message: "Exitoso", data: resultado }
        }
        res.status(200).json(jResponse);
    });
}

exports.actualizar = function(req, res) {

    let query = "";

    const { estatus, observaciones } = req.body;

    if (estatus == 'RECHAZADO' && observaciones) {

        query = `update prospectos set estatus= '${estatus}', observaciones = '${observaciones}' where id = ${req.params.id}`;
    } else {

        query = `update prospectos set estatus= 'AUTORIZADO', observaciones = '' where id = ${req.params.id} `;
    }
    console.log(query);
    conn.query(query, (err, resultado) => {

        if (err) {

            console.log("****************ERROR EN LA CONSULTA****************");
            console.log(err);
            console.log(query);
            console.log("****************ERROR EN LA CONSULTA****************");
            jResponse = { status: 400, message: "Ocurrió un error" }
        } else {
            jResponse = { status: 200, message: "Exitoso" }
        }
        res.status(200).json(jResponse);

    });


}