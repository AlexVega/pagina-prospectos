const client = require('./conection')
conn = client.createConnection();

//const shared = require('./shared');
let jResponse = {};


//GET Obtener postulante
exports.getProspectos = function(req, res) {

    const query = `SELECT id,nombre, apellidopaterno,apellidomaterno,calle, numero, colonia, codigo_postal, telefono, rfc, estatus, observaciones,documentos FROM prospectos`
    conn.query(query, (err, resultado) => {

        if (err) {

            console.log("****************ERROR EN LA CONSULTA****************");
            console.log(err);
            console.log(query);
            console.log("****************ERROR EN LA CONSULTA****************");
            jResponse = { status: 400, message: "Ocurrió un error" }
        } else {
            jResponse = { status: 200, message: "Exitoso", data: resultado }
        }
        res.status(200).json(jResponse);
    });
}

//POST => Crear nuevo.
exports.create = function(req, res) {
    console.log(req.body);
    const { nombre, apellidopaterno, apellidomaterno, calle, numero, colonia, codigo_postal, telefono, RFC, estatus, observaciones, documentos } = req.body;
    if (nombre && apellidopaterno && calle && numero && colonia && codigo_postal && telefono && RFC && estatus) {

        const query = `INSERT INTO prospectos(nombre,apellidopaterno,apellidomaterno,calle,numero,colonia,codigo_postal, telefono, rfc, estatus, observaciones, documentos)
     VALUES ('${nombre}', '${apellidopaterno}','${apellidomaterno}', '${calle}','${numero}','${colonia}','${codigo_postal}','${telefono}','${RFC}','${estatus}','${observaciones}','${documentos}')`;
        conn.query(query, (err, resultado) => {
            if (err) {

                console.log("****************ERROR EN LA CONSULTA****************");
                console.log(err);
                console.log(query);
                console.log("****************ERROR EN LA CONSULTA****************");
                jResponse = { status: 400, message: "Ocurrió un error" }
            } else {
                jResponse = { status: 200, message: "Exitoso" }
            }
            res.status(200).json(jResponse);

        });

    } else {
        jResponse = { status: 404, message: "La información no se envió correctamente" }
        res.status(200).json(jResponse);

    }
}