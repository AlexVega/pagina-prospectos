import { Component, OnInit } from '@angular/core';
import { SolicitudesService } from '../services/solicitudes.service';
import { Prospecto } from '../Clases/prospecto';

@Component({
  selector: 'app-evaluacion',
  templateUrl: './evaluacion.component.html',
  styleUrls: ['./evaluacion.component.css']
})
export class EvaluacionComponent implements OnInit {

  prospectos: Array<Prospecto> = new Array<Prospecto>();
  
  constructor(private api:SolicitudesService) { }

  async ngOnInit() {
    const data = await this.api.ObtenerProspectos();
    if (data.status == 200){

      this.prospectos= data.data;
    }else {
      alert("Ocurrió un error en el servicio");
    }
  }

}
