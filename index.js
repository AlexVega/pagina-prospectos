const express = require('express')
const app = express(),
    bodyParser = require("body-parser"),
    methodOverride = require("method-override"),
    cors = require('cors');


/*habilitar middleware */
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride("_method"));
app.use(express.static(__dirname + '/public'));
app.use(cors());


/* Add routers */
const prospectos = require('./routes/prospectos.routes');
const listado = require('./routes/listado.routes');
const validacion = require('./routes/validacion.routes');


app.use('/prospectos', prospectos);
app.use('/listado', listado);
app.use('/validacion', validacion);



// Start server
app.listen(3000, function() {
    console.log("Running");
});